import { Injectable } from '@angular/core';
import {BehaviorSubject, map} from "rxjs";
import {HttpClient} from "@angular/common/http";

export interface Auth {
  token: string;
  displayName: string;
  role: string;
}


@Injectable({
  providedIn: 'root'
})
export class AuthService {
  auth$ = new BehaviorSubject<Auth | null>(null)

  constructor() {
    setTimeout(() => this.login(), 2000)
    setTimeout(() => this.logout(), 3000)
  }

  login() {
    // http
    const res: Auth = { token: 'efihwefjew', displayName: 'Mario', role: 'admin'}
    this.auth$.next(res)
  }

  logout() {
    this.auth$.next(null)
  }

  get isLogged$() {
    return this.auth$
      .pipe(
        map(res => !!res)
      )
  }


  get displayName$() {
    return this.auth$
      .pipe(
        map(auth => auth?.displayName),
        map(res => res?.toUpperCase())
      )
  }



}
