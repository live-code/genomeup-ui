import { Component } from '@angular/core';
import {AuthService} from "../services/auth.service";
import {map} from "rxjs";

@Component({
  selector: 'app-navbar',
  template: `
    <button [routerLink]="'login'">login</button>
    <button routerLink="home">home</button>
    <button routerLink="catalog">catalog</button>
    <button routerLink="uikit1" routerLinkActive="bg-dark text-white">uikit1</button>
    <button routerLink="uikit2" routerLinkActive="bg-dark text-white">uikit2</button>
    <button routerLink="uikit3" routerLinkActive="bg-dark text-white">uikit3</button>
    <button routerLink="uikit4" routerLinkActive="bg-dark text-white">uikit4</button>
    <button routerLink="uikit5" appMyRouterLink="bg-warning">uikit5</button>
     <button *appIfSignin>logout</button>
    <div>{{authService.displayName$ | async}}</div>
  `,
})
export class NavbarComponent {
  constructor(public authService: AuthService) {
  }

}
