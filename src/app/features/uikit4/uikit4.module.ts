import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Uikit4RoutingModule } from './uikit4-routing.module';
import { Uikit4Component } from './uikit4.component';
import {SharedModule} from "../../shared/shared.module";
import {FormsModule} from "@angular/forms";


@NgModule({
  declarations: [
    Uikit4Component
  ],
  imports: [
    CommonModule,
    Uikit4RoutingModule,
    SharedModule,
    FormsModule
  ]
})
export class Uikit4Module { }
