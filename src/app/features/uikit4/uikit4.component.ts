import { Component } from '@angular/core';

@Component({
  selector: 'app-uikit4',
  template: `
    <div appBorder>lorem ipsum</div>


    <div>
      Lorem ipsum  <span appAlert>dolor sit amet</span>, consectetur adipisicing elit. Accusantium amet at culpa cumque dolorum illo molestias quo sunt, velit voluptatibus! Adipisci iste nam natus obcaecati quasi quidem tempora unde, vero.
    </div>

    <button (click)="value = 'danger'">danger</button>
    <span appAlert>ciao</span>


    <button url="http://www.google.com">Google</button>

    <div style="background-color: blue; padding: 30px" (click)="parent()">
      <div style="background-color: red; padding: 30px"
             (click)="child()" appStopPropagation>
            lorem
      </div>
    </div>

  `,
})
export class Uikit4Component {

  value = 'success'

  child() {
    console.log('child',)
  }

  parent() {
    console.log('parent')
  }
}
