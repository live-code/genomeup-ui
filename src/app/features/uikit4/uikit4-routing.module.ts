import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Uikit4Component } from './uikit4.component';

const routes: Routes = [{ path: '', component: Uikit4Component }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Uikit4RoutingModule { }
