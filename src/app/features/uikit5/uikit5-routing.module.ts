import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Uikit5Component } from './uikit5.component';

const routes: Routes = [{ path: '', component: Uikit5Component }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Uikit5RoutingModule { }
