import {Component, ElementRef, TemplateRef, ViewChild, ViewContainerRef} from '@angular/core';
import {catchError, combineLatest, forkJoin, fromEvent, interval, map, merge, of, take, takeUntil} from "rxjs";
import {FormControl} from "@angular/forms";
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'app-uikit5',
  template: `
    <button #btn>CLICK</button>
    <input type="text" [formControl]="input">
  `,
})
export class Uikit5Component {
  input = new FormControl()
  @ViewChild('btn') button!: ElementRef<HTMLButtonElement>

  constructor(private http: HttpClient) {}

  ngAfterViewInit() {
    const users$ = this.http.get<any[]>('https://jsonplaceholder.typicode.com/usersX')
        .pipe(
          catchError(err => {
            return of([] as any[])
          }),
        )

    const posts$ = this.http.get<any[]>('https://jsonplaceholder.typicode.com/posts')
      .pipe(
        catchError(err => {
          return of([] as any[])
        }),
      );

    combineLatest({users: users$, posts: posts$})
      .pipe(
        map(res => res.users.concat(res.posts))
      )
      .subscribe({
        next: (res) => {
          console.log('--->', res)
        },
        error: (err) => {
          console.log('ahia', err)
        }
      })

  }
}
