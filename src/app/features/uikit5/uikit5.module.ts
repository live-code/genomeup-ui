import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Uikit5RoutingModule } from './uikit5-routing.module';
import { Uikit5Component } from './uikit5.component';
import {ReactiveFormsModule} from "@angular/forms";


@NgModule({
  declarations: [
    Uikit5Component
  ],
  imports: [
    CommonModule,
    Uikit5RoutingModule,
    ReactiveFormsModule
  ]
})
export class Uikit5Module { }
