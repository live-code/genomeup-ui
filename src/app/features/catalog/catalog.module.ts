import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {CatalogComponent} from "./catalog.component";
import {CatalogListComponent} from "./components/catalog-list.component";
import {CatalogFormComponent} from "./components/catalog-form.component";
import {RouterModule, Routes} from "@angular/router";
import { CatalogHelpComponent } from './catalog-help.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {CatalogService} from "./services/catalog.service";
import {CardComponent} from "../../shared/components/card/card.component";
import {SharedModule} from "../../shared/shared.module";
import {CardModule} from "../../shared/components/card/card.module";

@NgModule({
  declarations: [
    CatalogComponent,
    CatalogListComponent,
    CatalogFormComponent,
    CatalogHelpComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    CardModule,
    RouterModule.forChild([
      {path: '', component: CatalogComponent},
      {path: 'help', component: CatalogHelpComponent},
    ]),
    ReactiveFormsModule
  ],
  providers: [
    CatalogService
  ]
})
export class CatalogModule { }
