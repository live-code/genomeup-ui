import {ChangeDetectionStrategy, Component} from '@angular/core';
import {CatalogService} from "../services/catalog.service";
import {FormControl} from "@angular/forms";

@Component({
  selector: 'app-catalog-form',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <input type="text" [formControl]="input">
    <button (click)="catalogService.addUser({ name: input.value})">ADD</button>

    {{render()}}
  `,
})
export class CatalogFormComponent {
  input = new FormControl;
  constructor(public catalogService: CatalogService) {}

  render() {
    console.log('render catalog form')
  }
}
