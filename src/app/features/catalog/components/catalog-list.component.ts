import {ChangeDetectionStrategy, Component, EventEmitter, Input, Output} from '@angular/core';
import {User} from "../../../model/user";

@Component({
  selector: 'app-catalog-list',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `

    <li *ngFor="let user of users">
      {{user.name}}
      <button (click)="deleteUser.emit(user.id)">Delete</button>
    </li>
  `,
})
export class CatalogListComponent {
  @Input() users: User[] = [];
  @Output() deleteUser = new EventEmitter<number>();
}

