import {Injectable} from "@angular/core";
import {User} from "../../../model/user";
import {HttpClient} from "@angular/common/http";

@Injectable()
export class CatalogService {
  users: User[] = []

  constructor(private http: HttpClient) {
    console.log('catalog service')
  }

  getUsers() {
    this.http.get<User[]>('https://jsonplaceholder.typicode.com/users')
      .subscribe(res => {
        this.users = res;
      })
  }

  addUser(formData: any) {
    this.http.post<User>('https://jsonplaceholder.typicode.com/users', formData)
      .subscribe(res => {
       //  this.users.push(res)
        this.users = [...this.users, res]
      })
  }

  deleteUser(id: number) {
    this.http.delete(`https://jsonplaceholder.typicode.com/users/${id}` )
      .subscribe(() => {
        const index = this.users.findIndex(u => u.id === id)
        //  this.users.splice(index, 1)
        this.users = this.users.filter(u => u.id !== id)
      })

  }

  editUser(formData: any) {
    // http.patch
    //  .subscribe(res => {
/*      this.users = this.users.map(u => {
        return u.id === selected.id ? res : u
      })*/
    //  )
  }
}
