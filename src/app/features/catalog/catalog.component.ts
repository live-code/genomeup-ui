import {ChangeDetectionStrategy, Component} from "@angular/core";
import {CatalogService} from "./services/catalog.service";
import {CatalogListComponent} from "./components/catalog-list.component";

@Component({
  selector: 'app-catalog',
  template: `
    <h1>Catalog {{catalogService.users.length}}</h1>
    <app-catalog-form></app-catalog-form>
    <app-catalog-list
        [users]="catalogService.users"
        (deleteUser)="catalogService.deleteUser($event)"
    ></app-catalog-list>



  `
})
export class CatalogComponent {
  constructor(public catalogService: CatalogService) {
    this.catalogService.getUsers()
  }

}

