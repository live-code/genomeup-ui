import { Component } from '@angular/core';
import {CatalogService} from "../catalog/services/catalog.service";

@Component({
  selector: 'app-home',
  template: `

    <app-static-map city="Rome"></app-static-map>
    <app-static-map
      city="Trieste"
      [zoom]="zoomValue"></app-static-map>

    <button (click)="zoomValue = zoomValue - 1">-</button>
    <button (click)="zoomValue = zoomValue + 1">+</button>

    <app-card
      title="Fabio"
      variant="success"
      icon="fa fa-bluetooth"
      (iconClick)="visible = true"
    >
      <div class="body">
        <input type="text">
        <input type="text">
        <input type="text">
        <input type="text">
      </div>

      <div class="footer">
        copryright
      </div>
    </app-card>

    <app-card
      *ngIf="visible"
      title="Fabio"
      headerCls="bg-success"
      icon="fa fa-link"
    >
      <div class="body">
        google map
      </div>

      <div class="footer">
        <button>1</button>
        <button>2</button>
        <button>3</button>
      </div>
    </app-card>


  `,
})
export class HomeComponent {
  zoomValue = 10;
  visible = false;
}
