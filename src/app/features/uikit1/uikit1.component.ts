import { Component } from '@angular/core';
import {City, Country} from "../../model/country";

@Component({
  selector: 'app-uikit1',
  template: `
    <app-tabbar
      [items]="countries"
      [(active)]="selectedCountry"
      (tabClick)="selectCountryHandler($event)"
    ></app-tabbar>

    <app-tabbar
      *ngIf="selectedCountry"
      labelField="label"
      [active]="selectedCity"
      [items]="selectedCountry.cities"
      (tabClick)="selectCityHandler($event)"
    ></app-tabbar>

    <app-static-map
      *ngIf="selectedCity"
      [city]="selectedCity.label"></app-static-map>

  `,
})
export class Uikit1Component {
  countries: Country[] = []
  selectedCountry: Country | undefined;
  selectedCity: City | undefined;

  constructor() {
    setTimeout(() => {
      this.countries = [
        {
          id: 1,
          name: 'Italy',
          cities: [
            { id: 1001, label: 'Milan', coords: { lat: 41, lng: 12 } },
            { id: 1002, label: 'Rome', coords: { lat: 42, lng: 13 } } ,
            { id: 1003, label: 'Milan', coords: { lat: 43, lng: 14 } }
          ]

        },
        {
          id: 2,
          name: 'Germany',
          cities: [
            { id: 1001, label: 'Berlino', coords: { lat: 55, lng: 33 } },
          ]
        },
        {
          id: 3,
          name: 'UK',
          cities: [
            { id: 1004, label: 'London', coords: { lat: 66, lng: 22 } },
            { id: 1007, label: 'Leeds', coords: { lat: 66, lng: 22 } },
          ]
        },
      ];
      this.selectCountryHandler(this.countries[0])
    }, 500)
  }

  selectCountryHandler(country: Country) {
    this.selectedCountry = country;
    this.selectedCity = this.selectedCountry.cities[0]
  }
  selectCityHandler(city: City) {
    this.selectedCity = city;
  }
}
