import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Uikit1RoutingModule } from './uikit1-routing.module';
import { Uikit1Component } from './uikit1.component';
import {TabbarComponent} from "../../shared/components/tabbar.component";
import {SharedModule} from "../../shared/shared.module";


@NgModule({
  declarations: [
    Uikit1Component
  ],
  imports: [
    CommonModule,
    Uikit1RoutingModule,
    TabbarComponent,
    SharedModule
  ]
})
export class Uikit1Module { }
