import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login.component';
import { SigninComponent } from './components/signin.component';
import { RegistrationComponent } from './components/registration.component';
import { LostPassComponent } from './components/lost-pass.component';


@NgModule({
  declarations: [
    LoginComponent,
    SigninComponent,
    RegistrationComponent,
    LostPassComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '', component: LoginComponent,
        children: [
          { path: 'registration', component: RegistrationComponent },
          { path: 'signin', component: SigninComponent },
          { path: 'lostpass', component: LostPassComponent },
          { path: '', redirectTo: 'signin', pathMatch: 'full'}
        ]
      },
    ])
  ]
})
export class LoginModule { }
