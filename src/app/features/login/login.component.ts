import { Component } from '@angular/core';

@Component({
  selector: 'app-login',
  template: `
    <router-outlet></router-outlet>
    <hr>
    <button routerLink="signin">signin</button>
    <button routerLink="registration">registration</button>
    <button routerLink="lostpass">lost password</button>

  `,
  styles: [
  ]
})
export class LoginComponent {

}
