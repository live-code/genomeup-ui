import {Component, QueryList, ViewChildren} from '@angular/core';
import {GroupComponent} from "../../shared/components/group.component";
import * as L from "leaflet";

@Component({
  selector: 'app-uikit3',
  template: `
    <app-accordion>
      <app-group title="1">blabla</app-group>
      <app-group title="2">blabla</app-group>
      <app-group title="3">blabla</app-group>
      <app-group title="4">blabla</app-group>
    </app-accordion>

    <app-leaflet [coords]="[43, 13]"
                 [zoom]="zoomValue"></app-leaflet>

    <button (click)="zoomValue = zoomValue - 1">-</button>
    <button (click)="zoomValue = zoomValue + 1">+</button>

    <div class="row">
      <div class="col-sm">1</div>
      <div class="col-sm">2</div>
      <div class="col-sm">3</div>
    </div>

    <app-row mq="sm">
      <app-col>left</app-col>
      <app-col>center</app-col>
      <app-col>right</app-col>
    </app-row>

    <div *appPad="1, PIPPO '1234'">PAD</div>
    <div appPad="2">PAD</div>
    <div appPad="3">PAD</div>

  `,

})
export class Uikit3Component {
  zoomValue = 10
}
