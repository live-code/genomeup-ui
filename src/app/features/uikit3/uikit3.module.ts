import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Uikit3RoutingModule } from './uikit3-routing.module';
import { Uikit3Component } from './uikit3.component';
import {SharedModule} from "../../shared/shared.module";
import {RowComponent} from "../../shared/components/row.component";
import {ColComponent} from "../../shared/components/col.component";


@NgModule({
  declarations: [
    Uikit3Component
  ],
  imports: [
    CommonModule,
    Uikit3RoutingModule,
    SharedModule,
    RowComponent,
    ColComponent
  ]
})
export class Uikit3Module { }
