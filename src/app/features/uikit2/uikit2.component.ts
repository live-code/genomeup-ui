import {AfterViewInit, Component, ElementRef, QueryList, ViewChild, ViewChildren} from '@angular/core';
import {CardComponent} from "../../shared/components/card/card.component";
import {StaticMapComponent} from "../../shared/components/static-map/static-map.component";

@Component({
  selector: 'app-uikit2',
  template: `
    <input type="text" #inputRef>
  `,
})
export class Uikit2Component implements AfterViewInit{
  @ViewChild('inputRef')  input!: ElementRef<HTMLInputElement>;
  @ViewChildren(CardComponent) cards!: QueryList<any>


  ngAfterViewInit() {
      this.cards.toArray().forEach((c, index) => {
        c.title = 'title'  + index
      })

  }
}
