import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Uikit2RoutingModule } from './uikit2-routing.module';
import { Uikit2Component } from './uikit2.component';
import {SharedModule} from "../../shared/shared.module";


@NgModule({
  declarations: [
    Uikit2Component
  ],
  imports: [
    CommonModule,
    Uikit2RoutingModule,
    SharedModule
  ]
})
export class Uikit2Module { }
