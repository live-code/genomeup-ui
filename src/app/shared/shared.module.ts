import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {CardModule} from "./components/card/card.module";
import {StaticMapModule} from "./components/static-map/static-map.module";
import {TabbarComponent} from "./components/tabbar.component";
import {LeafletComponent} from "./components/leaflet.component";
import {AccordionComponent} from "./components/accordion.component";
import {GroupComponent} from "./components/group.component";
import { PadDirective } from './directives/pad.directive';
import { BorderDirective } from './directives/border.directive';
import { AlertDirective } from './directives/alert.directive';
import { UrlDirective } from './directives/url.directive';
import { StopPropagationDirective } from './directives/stop-propagation.directive';
import { MyRouterLinkDirective } from './directives/my-router-link.directive';
import { IfSigninDirective } from './directives/if-signin.directive';


@NgModule({
  imports: [
    CommonModule,
    CardModule,
    StaticMapModule,
    TabbarComponent,
    LeafletComponent,
    AccordionComponent,
    GroupComponent
  ],
  exports: [
    CardModule,
    StaticMapModule,
    TabbarComponent,
    LeafletComponent,
    AccordionComponent,
    GroupComponent,
    PadDirective,
    BorderDirective,
    AlertDirective,
    UrlDirective,
    StopPropagationDirective,
    MyRouterLinkDirective,
    IfSigninDirective
  ],
  declarations: [
    PadDirective,
    BorderDirective,
    AlertDirective,
    UrlDirective,
    StopPropagationDirective,
    MyRouterLinkDirective,
    IfSigninDirective
  ]
})
export class SharedModule { }
