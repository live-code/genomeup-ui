import {Directive, HostBinding, TemplateRef, ViewContainerRef} from '@angular/core';
import {AuthService} from "../../core/services/auth.service";
import {map, Subject, Subscription, takeUntil} from "rxjs";

@Directive({
  selector: '[appIfSignin]'
})
export class IfSigninDirective {
  destroy$ = new Subject();

  constructor(
    private template: TemplateRef<any>,
    private view: ViewContainerRef,
    private authSrv: AuthService
  ) {
    this.authSrv.isLogged$
      .pipe(
        takeUntil(this.destroy$)
      )
      .pipe(

      )
      .subscribe(islogged => {
        if (islogged) {
          view.createEmbeddedView(template)
        } else {
          view.clear()
        }
      })

  }

  ngOnDestroy() {
    this.destroy$.next(null)
    this.destroy$.complete()
  }
}
