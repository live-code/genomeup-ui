import {Directive, ElementRef, HostBinding, Input, Renderer2, SimpleChanges} from '@angular/core';

@Directive({
  selector: '[appAlert]'
})
export class AlertDirective {
  @Input() set appAlert(val: string) {
    this.renderer.addClass(this.el.nativeElement, `alert`)
    this.renderer.addClass(this.el.nativeElement, `alert-${val || 'info'}`)
  }

  constructor(
    private el: ElementRef<HTMLElement>,
    private renderer: Renderer2
  ) {
    console.log(el.nativeElement)
  }

  ngOnChanges(changes: SimpleChanges) {

  }

}

