import {Directive, ElementRef, HostListener, Input} from '@angular/core';

@Directive({
  selector: '[url]'
})
export class UrlDirective {
  @Input({ required: true }) url: string | undefined

  @HostListener('click')
  clickMe() {
    window.open(this.url!)
  }

}
