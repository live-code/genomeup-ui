import {Directive, ElementRef, Input, Renderer2} from '@angular/core';
import {ActivationEnd, NavigationEnd, Router, RouterLink, RouterStateSnapshot} from "@angular/router";
import {filter} from "rxjs";

@Directive({
  selector: '[appMyRouterLink]'
})
export class MyRouterLinkDirective {
  @Input() appMyRouterLink: string = '';

  constructor(
    private router: Router,
    private el: ElementRef<any>,
    private renderer: Renderer2,
    private routerLink: RouterLink
  ) {
    const routerLinkUrl = this.el.nativeElement.getAttribute('routerLink');
    router.events
      .pipe(
        filter(ev => ev instanceof NavigationEnd )
      )
      .subscribe((ev) => {
        const url = (ev as NavigationEnd).url
        if (url.includes(routerLinkUrl)) {
          this.renderer.addClass(this.el.nativeElement, this.appMyRouterLink)
        } else {
          this.renderer.removeClass(this.el.nativeElement, this.appMyRouterLink)
        }
      })

  }

}
