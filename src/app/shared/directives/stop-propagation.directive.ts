import {Directive, HostListener} from '@angular/core';

@Directive({
  selector: '[appStopPropagation]'
})
export class StopPropagationDirective {

  @HostListener('click', ['$event'])
  stopProp(e: MouseEvent) {
    e.stopPropagation()
  }
  constructor() { }

}
