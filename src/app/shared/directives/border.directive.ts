import {Directive, HostBinding} from '@angular/core';

@Directive({
  selector: '[appBorder]'
})
export class BorderDirective {
  @HostBinding('style.border')
  get border() {
    return '3px solid blue'
  }


}
