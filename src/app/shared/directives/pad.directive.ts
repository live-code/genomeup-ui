import {Directive, HostBinding, Input} from '@angular/core';

@Directive({
  selector: '[appPad]'
})
export class PadDirective {
  @HostBinding() get class() {
    return 'p-' + this.appPad
  }
  @Input() appPad: any = '';

  @Input() set appPadPIPPO(val: any) {
    console.log(val)
  }

}
