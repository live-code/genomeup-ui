import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-card',
  template: `
    <div class="card">
      <div
        class="card-header"
        [ngClass]="{
            'bg-success': variant === 'success',
            'bg-danger': variant === 'failed'
        }"
      >
        <div class="d-flex justify-content-between align-items-center">
          <div>{{title}}</div>
          <i *ngIf="icon"
             (click)="iconClick.emit()"
             [class]="icon"></i>
        </div>
      </div>
      <div class="card-body">
        <ng-content select=".body"></ng-content>
      </div>

      <div class="card-footer">
        <ng-content select=".footer"></ng-content>
      </div>
    </div>
  `,
})
export class CardComponent {
  @Input()
  title: string | number = ''

  @Input() icon: string | undefined;
  @Input() headerCls: string = ''
  @Input() variant: 'success' | 'failed' | undefined;

  @Output() iconClick = new EventEmitter()
}
