import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StaticMapComponent } from './static-map.component';



@NgModule({
  declarations: [
    StaticMapComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    StaticMapComponent
  ]
})
export class StaticMapModule { }
