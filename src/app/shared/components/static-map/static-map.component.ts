import {ChangeDetectionStrategy, Component, Input, OnChanges} from '@angular/core';

@Component({
  selector: 'app-static-map',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <h1>{{city}}</h1>
    <img
      width="100%"
      [src]="url" alt="">
  `,
})
export class StaticMapComponent implements OnChanges {
  @Input() city: string | undefined;
  @Input() zoom = 10;

  url = '';

  ngOnChanges() {
    this.url = 'https://www.mapquestapi.com/staticmap/v5/map?key=Go3ZWai1i4nd2o7kBuAUs4y7pnpjXdZn&center='+ this.city +'&size=400,200&zoom=' + this.zoom
  }
}
