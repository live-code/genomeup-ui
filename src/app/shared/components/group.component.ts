import {Component, EventEmitter, Input, Output} from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-group',
  standalone: true,
  imports: [CommonModule],
  template: `
    <div class="card">
      <div
        class="card-header"
        (click)="headerClick.emit()"
      >
          {{title}}
      </div>
      <div class="card-body" *ngIf="isOpen">
        <ng-content></ng-content>
      </div>
    </div>
  `,
  styles: [
  ]
})
export class GroupComponent {
  @Input() isOpen = false
  @Input() title = '';
  @Output() headerClick = new EventEmitter()
}
