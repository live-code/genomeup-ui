import {Component, HostBinding, Optional} from '@angular/core';
import { CommonModule } from '@angular/common';
import {RowComponent} from "./row.component";

@Component({
  selector: 'app-col',
  standalone: true,
  imports: [CommonModule],
  template: `
      <ng-content></ng-content>
  `,
})
export class ColComponent {
  @HostBinding() get class() {
    return 'col-' + this.parent.mq
  }

  constructor(@Optional() private parent: RowComponent  ) {
    if (!this.parent) {
      throw new Error('col must be used inside row')
    }
  }
}
