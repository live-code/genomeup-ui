import {
  AfterContentInit,
  Component,
  ContentChild,
  ContentChildren,
  QueryList,
  ViewChild,
  ViewChildren
} from '@angular/core';
import { CommonModule } from '@angular/common';
import {GroupComponent} from "./group.component";

@Component({
  selector: 'app-accordion',
  standalone: true,
  imports: [CommonModule],
  template: `
    <div style="border: 4px solid black">
     <ng-content></ng-content>
    </div>
  `,
})
export class AccordionComponent implements AfterContentInit{
  @ContentChildren(GroupComponent) groups!: QueryList<GroupComponent>

  ngAfterContentInit() {
    const first =this.groups.first;

    if(!(first instanceof GroupComponent))
      throw new Error('First child should  be a Group Component')

    first.isOpen = true

    this.groups.toArray().forEach(g => {
      g.headerClick.subscribe(() => {
        this.closeAll();
        g.isOpen = true
      })
    })
  }

  closeAll() {
    this.groups.toArray().forEach(g => {
      g.isOpen = false
    })
  }
}
