import {Component, Input} from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-row',
  standalone: true,
  imports: [CommonModule],
  template: `
    <div class="row">
        <ng-content></ng-content>
    </div>
  `,
  styles: [
  ]
})
export class RowComponent {
  @Input() mq: 'sm' | 'md' | 'lg' = 'sm'
}
