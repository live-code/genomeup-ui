import {Component, ElementRef, Input, SimpleChanges, ViewChild} from '@angular/core';
import { CommonModule } from '@angular/common';
import * as L from "leaflet";
import {LatLngExpression} from "leaflet";
import {LoginComponent} from "../../features/login/login.component";

@Component({
  selector: 'app-leaflet',
  standalone: true,
  imports: [CommonModule],
  template: `
    <div #host class="map"></div>
  `,
  styles: [`
    .map { height: 180px; }
  `]
})
export class LeafletComponent {
  @ViewChild('host', { static: true }) host!: ElementRef<HTMLDivElement>
  @Input() zoom: number = 10;
  @Input({ required: true }) coords!: LatLngExpression
  map!: L.Map

  ngOnChanges(changes: SimpleChanges) {
    if (!this.map) {
      this.init()
    };

    if (changes['zoom']) {
      this.map.setZoom(this.zoom)
    }
  }

  init() {
    this.map = L.map(this.host.nativeElement)
      .setView(this.coords, this.zoom);
    L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
      maxZoom: 19,
    }).addTo(this.map);

    const marker = L.marker(this.coords).addTo(this.map);


  }



}
