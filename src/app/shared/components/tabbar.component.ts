import {Component, EventEmitter, Input, Output} from '@angular/core';
import {CommonModule} from '@angular/common';

@Component({
  selector: 'app-tabbar',
  standalone: true,
  imports: [CommonModule],
  template: `
    <ul class="nav nav-tabs">
      <li
        class="nav-item"
        *ngFor="let item of items"
        (click)="tabClickHandler(item)"
      >
        <a
          class="nav-link"
          [ngClass]="{'active': item.id === active?.id}"
        >
            {{item[labelField]}}
        </a>
      </li>

    </ul>

  `,
})
export class TabbarComponent<T extends { id: number, [key: string]: any}> {
  @Input() items: T[]  = []
  @Input() active: T | undefined;
  @Input() labelField: string = 'name'
  @Output() tabClick = new EventEmitter<T>()
  @Output() activeChange = new EventEmitter<T>();

  tabClickHandler(item: any) {
     this.activeChange.emit(item)
     this.tabClick.emit(item)
  }
}

