import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from "@angular/router";



const routes: Routes = [
  {
    path: 'catalog',
    loadChildren: () => import('./features/catalog/catalog.module').then(m => m.CatalogModule)
  },
  {
    path: 'home',
    loadChildren: () => import('./features/home/home.module').then(m => m.HomeModule)
  },
  {path: 'login', loadChildren: () => import('./features/login/login.module').then(m => m.LoginModule)},
  {
    path: '',
    redirectTo: 'catalog',
    pathMatch: 'full'
  },
  { path: 'uikit1', loadChildren: () => import('./features/uikit1/uikit1.module').then(m => m.Uikit1Module) },
  { path: 'uikit2', loadChildren: () => import('./features/uikit2/uikit2.module').then(m => m.Uikit2Module) },
  { path: 'uikit3', loadChildren: () => import('./features/uikit3/uikit3.module').then(m => m.Uikit3Module) },
  { path: 'uikit4', loadChildren: () => import('./features/uikit4/uikit4.module').then(m => m.Uikit4Module) },
  { path: 'uikit5', loadChildren: () => import('./features/uikit5/uikit5.module').then(m => m.Uikit5Module) },
  {
    path: '**',
    redirectTo: 'home'
  }
]
@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes),
  ],
  exports: [
    RouterModule,
  ]
})
export class AppRoutingModule { }
